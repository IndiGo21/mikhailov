package com.example.developerslife

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: MainViewModel

    private val retrofitService = RetrofitService.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(MainRepository(retrofitService))
        ).get(MainViewModel::class.java)

        viewModel.videoList.observe(this, Observer {
            Glide.with(this)
                .asGif()
                .load(it.last().gifURL)
                .thumbnail(
                    Glide.with(this).asGif().load(R.drawable.loader)
                )
                .into(gif)
            description.text = it.last().description
        })

        viewModel.error.observe(this, Observer {
            if (it) {
                Glide.with(this)
                    .load(R.drawable.no_internet)
                    .into(gif)
                description.text =
                    "Произошла ошибка при загрузке данных. Проверьте подключение к сети"
                next.text = "Попробовать снова"
                previous.visibility = View.INVISIBLE
            }
            else{
                next.text = "Следующий"
                previous.visibility = View.VISIBLE
            }
        })

        viewModel.showButton.observe(this, Observer {
            if (it) {
                previous.visibility = View.VISIBLE
            }
            else{
                previous.visibility = View.INVISIBLE
            }
        })

        next.setOnClickListener {
            viewModel.getNextVideo()
        }
        previous.setOnClickListener {
            viewModel.getPreviousVideo()
        }
    }
}