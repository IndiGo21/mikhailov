package com.example.developerslife

class MainRepository(private val retrofitService: RetrofitService) {
    fun getNextVideo() = retrofitService.getNextVideo()
}