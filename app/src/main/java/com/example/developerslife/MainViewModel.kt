package com.example.developerslife

import android.net.Uri
import android.util.Log
import androidx.core.net.toUri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(private val repository: MainRepository):  ViewModel() {

    val videoList = MutableLiveData<List<VideoModel>>()
    val showButton = MutableLiveData<Boolean>()
    private val videoList1 = ArrayList<VideoModel>()
    val error = MutableLiveData<Boolean>()

    fun getNextVideo() {
        val response = repository.getNextVideo()
        response.enqueue(object : Callback<VideoModel> {
            override fun onResponse(call: Call<VideoModel>, response: Response<VideoModel>) {
                error.postValue(false)
                videoList1.add(response.body()!!)
                videoList.postValue(videoList1)
                buttonCheck()

            }
            override fun onFailure(call: Call<VideoModel>, t: Throwable) {
                error.postValue(true)
            }
        })
    }

    fun getPreviousVideo(){
        if (!error.value!!){
            videoList1.removeLast()
            videoList.postValue(videoList1)
            buttonCheck()
        }
        else {
            videoList.postValue(videoList1)
            videoList1.removeLast()
            buttonCheck()
        }

    }

    fun buttonCheck(){
        if (videoList1.size > 1 ){
            showButton.postValue(true)
        }
        else {
            showButton.postValue(false)
        }
    }

    init {
        getNextVideo()

    }


}